﻿using System;
using UnityEngine;
using System.Collections.Generic;

	public class entityPlayer:entityActor
	{
	//public int SelectedWeapon=0;
	public int[] Ammo;
	public int[] Clips;
	public int Level=0;

	public propertyWeapon Weapon;

	public override float getHeight()
	{
		return 0.4f;
	}


	public entityAI Brain;
	public override bool isPlayer()
	{
		return Brain==null;
	}

	public override void onCreated ()
	{
		Radius = 0.25f;

		gameObject = new GameObject ();
		gameObject.name = GetName ();
		gameObject.transform.localScale *= GetSpriteScale ();

		gameObject.AddComponent < SpriteRenderer> ();

		Weapon = propertyWeapon.Custom (propertyWeapon.WeaponType.SquirtGun,false);

		DrawSprites ("Players/Player" + alignment);
		base.onCreated ();
	}

	public entityPlayer()
	{

	}

	public entityPlayer (StateInGame gData, int side,float yDelta)
	{
		game = gData;
		alignment = side;

		if (alignment == 0) {
			Position = new Vector3 (2, game.getLimits().height/2f+yDelta);
			squirtColor = Color.red;
		} else {
			Position = new Vector3(game.getLimits().width-2,game.getLimits().height/2f+yDelta);
			squirtColor = Color.blue;
		}
		onCreated ();
	}

	public override float GetElasticity ()
	{
		return 0;
	}

	public override string GetName ()
	{
		return "Player";
	}

	/* UNUSED	public float GetDashTime()
	{
		switch (Character)
		{
		case 0://Cadet
			return 0.2f;
		case 1://Spaz
			return 1.0f;
		case 2://Kid
			return 3.0f;
		case 3://Robot
			return 0.0f;
		case 4://Creature
			return 4.00f;
		case 5://Angel
			return 2.00f;
		}
		return 0.4f;
	}

	public void DoDash()
	{
		if (!HasModifier ("dashCD") && !HasModifier ("Jump")) {

			switch (Character)
			{
			case 0://Cadet
				Vector2 Direction = new Vector2 (
					                   Input.GetAxisRaw ("Horizontal"),
					                   Input.GetAxisRaw ("Vertical")
				                   );

				if (Direction != Vector2.zero) {
					Velocity.x = Direction.y * GetDashSpeed () * Mathf.Sin (Rotation / 180 * Mathf.PI) + Direction.x * GetDashSpeed () * Mathf.Sin ((Rotation + 90) / 180 * Mathf.PI);
					Velocity.y = Direction.y * GetDashSpeed () * Mathf.Cos (Rotation / 180 * Mathf.PI) + Direction.x * GetDashSpeed () * Mathf.Cos ((Rotation + 90) / 180 * Mathf.PI);


					AddModifier (game, new propertyModifier ("Dash", propertyModifier.Allignment_neutral, GetDashTime (), new int[]{ }, new float[][]{ new float[] {
							propertyModifier.property_friction,
							0
						} }), 0);
					AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () * 5, new int[]{ }), 0);
				}
				break;

			case 1://Spaz
				AddModifier (game, new propertyModifier ("Dash", propertyModifier.Allignment_neutral, GetDashTime (), new int[]{ }, new float[][]{ new float[] {
						propertyModifier.property_time,
						0.5f
					} }), 0);
				AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () * 5, new int[]{ }), 0);
				break;
			case 2://Kid

				AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () , new int[]{ }), 0);
				break;
			case 3://Robot
				FireWeapon(SelectedWeapon);
				break;
			case 4://Creature
				AddModifier (game, new propertyModifier ("Dash", propertyModifier.Allignment_neutral, GetDashTime (), new int[]{ propertyModifier.modstate_Flying,propertyModifier.modstate_Invulnerable,propertyModifier.modstate_Disarmed,propertyModifier.modstate_Unstoppable }, new float[][]{ }), 0);
				AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () * 5, new int[]{ }), 0);
				break;
			case 5://Angel
				AddModifier (game, new propertyModifier ("Dash", propertyModifier.Allignment_neutral, GetDashTime (), new int[]{ propertyModifier.modstate_Invulnerable }, new float[][]{ }), 0);
				AddModifier (game, new propertyModifier ("dashCD", propertyModifier.Allignment_neutral, GetDashTime () * 5, new int[]{ }), 0);
				break;
			} 
		}
	}*/

	public void FireWeapon(){
		if (CanAttack() && Weapon!=null){
			{
				Rotation = Mathf.Atan2 (input.z, input.w) * 180f / Mathf.PI;

				Weapon.Fire (game, this,Rotation);

				/* UNUSED if (HasModifier("reload")
				){
					propertyModifier turrmod = new propertyModifier ("Animate", 0, GetModifierByName ("reload").Duration / turret.MaxFrames, new int[0]);
					turret.AddModifier (game,turrmod , 2);
					turrmod.AddFunction (propertyModifier.actions.think, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						turret.Update();	
					});
					turrmod.thinkInterval = 0.2f;
					}*/
				}


			}
	}
	/* UNUSED	public override float GetFriction()
	{
		if (Character == 1) {
			return base.GetFriction ()*0.5f;
		}
		return base.GetFriction ();
	}*/

	public float GetMoveSpeed()
	{
		return 8;
	}

	public float GetDashSpeed()
	{
		return 6;
	}

	public Vector4 input=Vector4.zero;
	public virtual void HandleInput()
	{
		if (CanAct ()) {
			if (isPlayer ()) {
				input.x = Input.GetAxisRaw ("Horizontal");
				input.y = Input.GetAxisRaw ("Vertical");
				input.z = Input.GetAxisRaw ("Fire1");
				input.w = Input.GetAxisRaw ("Fire2");
					
			} else {
				Brain.Update ();
			}

			Vector2 vSpeed = new Vector2 (input.x * GetMoveSpeed (), input.y * GetMoveSpeed ());

				SetAcceleration (vSpeed.x, vSpeed.y);
				if (vSpeed != Vector2.zero) {
					if (!HasModifier ("Animate")) {
						AddModifier (game, new propertyModifier ("Animate", propertyModifier.Allignment_neutral, 1f / GetMoveSpeed (), new int[]{ }, new float[][]{ }), 2);
					}
				}

			if (new Vector2 (input.z, input.w).magnitude != 0) {


					FireWeapon ();
				}
				/* UNUSED	if (Input.GetAxisRaw ("ChangeWeapon") != 0) {

					if (lastWeaponChangeTimer < Time.time) {
						lastWeaponChangeTimer = Time.time + 0.3f;

						ChangeWeapon ();
					}
					//UNUSED DoDash ();
				}
			} */
		} else {
			Acceleration = Vector2.zero;}
	}

	public override Vector2 GetFlight()
	{
		return new Vector2 (-1, 0);
	}

	public override void Update ()
	{
		HandleInput ();
		base.Update ();
	}

	public float GetAirTime(){
		return 0.5f;
	}

	public override int GetFacing()
	{
		if (HasModifier ("dead")) {
			return Animation_Death+4;

		} else {

			float rAngle = (Rotation+45 ) % 360;

			if (rAngle < 0) {
				rAngle = 360 + rAngle;
			}

			int final = Mathf.FloorToInt ((rAngle) / 360f * MaxSides);

			if (HasModifier ("dash")) {
				final += 4;

			}
			return final;
		}
	}

	/* UNUSED	public void ChangeWeapon()
	{
		SelectedWeapon++;
		while (Weapons [SelectedWeapon].Clip == 0 && !Weapons [SelectedWeapon].HasInfiniteAmmo ()) {
			SelectedWeapon++;
		}
	}
*/
	public override float getPuddleRadius ()
	{
		return 1.25f;
	}

	public void AssignAI()
	{

		//switch (alignment) {

	//	case 0: // Dasher Randomly

			float DashTime = 0.33f;
			float DashSpeed = 2f;
			float ThinkInterval = 3f;

		Vector2 lastFollowPos = Position;

			Brain = new entityAI (
				this,
				ThinkInterval);

			Brain.AddFunction(entityAI.nThink,
				delegate {

				if (((Vector2)Position-lastFollowPos).magnitude<Radius*2){
					List<Vector2Int> enemyTiles = new List<Vector2Int>();
					for (int iX=0; iX<game.floor.TileSetData.GetLength(0); iX++){

						for (int iY=0; iY<game.floor.TileSetData.GetLength(1); iY++){
							if (game.floor.TileSetData[iX,iY]!=1+alignment)
							{
								enemyTiles.Add(new Vector2Int(iX,iY));
							}
						}
					}

					lastFollowPos=((Vector2)enemyTiles[(int)UnityEngine.Random.Range(0,enemyTiles.Count-1)])/(DrawTheWorld.DefaultTileSize*propertyFloor.FloorMulti);
				}

				Vector2 dir = (lastFollowPos-(Vector2)Position).normalized;
				input.x=dir.x;
				input.y=dir.y;
					/*Rotation = UnityEngine.Random.Range(0,360);

					Velocity = new Vector2(
						DashSpeed * Mathf.Sin (Rotation / 180 * Mathf.PI),
						DashSpeed * Mathf.Cos (Rotation / 180 * Mathf.PI)
					);

					propertyModifier dashMod=new propertyModifier ("Dash",propertyModifier.Allignment_neutral, DashTime, new int[]{ }, new float[][]{new float[] {propertyModifier.property_friction,0}});
					/* UNUSED dashMod.AddFunction(propertyModifier.actions.expire, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						
						game.generatePuddle (Owner.Position, 1f*DrawTheWorld.DefaultTileSize/2f,9, Owner.squirtColor, 1);

					});* /

					AddModifier (game, dashMod, 0);
					AddModifier (game, new propertyModifier ("Animate",propertyModifier.Allignment_neutral, DashTime, new int[]{ }, new float[][]{}), 0);*/
				}
			);

			//break;
		//}
	}

	public override void onCollide (float X, float Y)
	{
		if (!isPlayer ()) {
			Brain.ExecuteFunction (entityAI.nCollide);
		}
	}

	public override void onCollide (entityBase target)
	{
		base.onCollide (target);

		if (!isPlayer ()) {
			Brain.ExecuteFunction (entityAI.nHit);
			if (target.isPlayer ()) {

				Brain.ExecuteFunction (entityAI.nAttack);
			}
		}
	}
}