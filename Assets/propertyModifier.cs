﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class propertyModifier
{
	public string ModifierName;
	public float totDuration;
	public float Duration;
	public int[] ModifierStates;
	public float [][] ModifierProperties;
	public int Alignment=0;
	public int DispelLevel=0;
	public float thinkInterval = 0f;
	public enum actions {
		collide=0,
		attack=1,
		expire=2,
		think=3,
		boom=4,
		max=5
	};
	Timer thinker;

	public delegate void ModifierAction(propertyModifier Mod,entitySpecialEffect effect,entityActor target);
	public List<actions> FuncNames = new List<actions> ();
	public List<ModifierAction> functions = new List<ModifierAction>();

	public static int modstate_Stunned=0;//cannot act
	public static int modstate_Rooted=1;//cannot move
	public static int modstate_Disarmed=2;//cannot fire
	public static int modstate_Flying=3;//cannot fall
	public static int modstate_Ghost=4;//ignores walls
	public static int modstate_Invulnerable=5;//ignores damage
	public static int modstate_Phased=6;//Ignores collision
	public static int modstate_Unstoppable=7;//ignores knockback
	public static int modstate_Max=8;

	public static int dispel_weak=0;
	public static int dispel_strong=1;
	public static int dispel_absolute=2;

	public static int property_speed=0;
	public static int property_friction=1;
	public static int property_time=2;//player only
	public static int property_elasticity=2;
	public static int property_mass=3;
	public static int property_incomingdamage=4;
	public static int property_max=5;

	public static int Allignment_negative=0;
	public static int Allignment_positive=1;
	public static int Allignment_neutral=2;

	public propertyModifier(string Name,int alignment,float t,int[] States)
	{
		ModifierName = Name;
		totDuration = t;
		ModifierStates = States;
		ModifierProperties= new float[][]{};
		Alignment=alignment;
	}

	public propertyModifier(string Name,int alignment,float t, float[][] Props)
	{
		ModifierName = Name;
		totDuration = t;
		ModifierStates = new int[]{};
		ModifierProperties= Props;
		Alignment=alignment;
	}

	public propertyModifier(string Name,int alignment,float t,int[] States, float[][] Props)
	{
		ModifierName = Name;
		totDuration = t;
		ModifierStates = States;
		ModifierProperties= Props;
		Alignment=alignment;
	}

	public propertyModifier (string Name,int alignment,float t,int[] States, float[][] Props, ModifierAction aDeath ){

		ModifierName = Name;
		totDuration = t;
		ModifierStates = States;
		AddFunction(actions.expire,aDeath);
		Alignment=alignment;	
	}

	public void AddFunction(actions Name, ModifierAction execution)
	{
		if (execution == null) {
			return;}
		if (FuncNames.Contains (Name)) {
			functions[FuncNames.IndexOf (Name)]=execution;
		} else {
			FuncNames.Add (Name);
			functions.Add (execution);
		}
		if (Name == actions.think) {
			if (thinker == null) {
				thinker = Timer.Create (ModifierName, thinkInterval, delegate {
					FireFunction(actions.think);
					return thinkInterval;	
				}, delegate(Timer original) {
					return 0;
				});
			}
		}
	}

	public void FireFunction(actions Name){

		FireFunction (Name, null, null);
	}

	public void FireFunction(actions Name,entityActor target){

		FireFunction (Name, target, null);
	}

	public void FireFunction(actions Name,entitySpecialEffect effect){

		FireFunction (Name, null, effect);
	}

	public void FireFunction(actions Name,entityActor target,entitySpecialEffect effect){

		if (FuncNames.IndexOf (Name) >= 0 && FuncNames.IndexOf (Name) < FuncNames.Count) {
			if (functions[FuncNames.IndexOf (Name)] != null) {

				functions [FuncNames.IndexOf (Name)] (this,effect,target);
			}
		}
	}

	public bool dead=false;
	public void Die(bool fire)
	{
		if (fire) {
			FireFunction (actions.expire);
		}
		if (thinker != null) {
			thinker.Kill (false);
		}
		dead = true;
	}

	public float GetModifierDuration(StateInGame game)
	{
		if (totDuration >= 0) {
			return Duration - game.gameTime;
		}
		return (game.gameTime - Duration) % (totDuration);
	}
}