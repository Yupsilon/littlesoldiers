﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class entityBase
{
	public static float projMult = 1;
	public static int MaxFacings= 8; 
	public int MaxFrames=0;
	public static int MaxSides = 4;
	public Color32 squirtColor=Color.white;

	public StateInGame game=null;

	//Animation
	public List<Sprite> SpriteData;
	public float fFrame=0;
	public virtual bool isFixedRotation()
	{
		return false;
	}

	//Movement
	public Vector3 Position = Vector3.zero;
	public Vector2 Velocity = Vector2.zero;
	public Vector2 Acceleration = Vector2.zero;
	public float Radius = 0;
	public float Rotation = 0;

	public int ID = 0;	
	public int alignment = -1;	
	public bool isDead = false;
	public GameObject gameObject;

	public bool isFriendly(entityBase other)
	{
		return other.alignment == this.alignment;
	}

	public virtual void onCreated()
	{

	}
	public virtual float getHeight()
	{
		return 0f;
	}

	public void gotoTile(int iX, int iY)
	{
		Position = new Vector3 (
			iX+1/2f,
			iY+1/2f,
			Position.z
		);
	}


	public virtual bool TakeProjectiles()
	{return false;}

	public virtual void Update()
	{
		Move ();
	}

	public virtual bool CanAct()
	{
		return game.GetSpeed()>0 && !isDead;
	}

	public virtual float GetFriction(){

		return 1f;}
	public void Halt()
	{
		Velocity = Vector2.zero;
		Acceleration = Vector2.zero;
	}

	public virtual bool isDiscardable()
	{
		return false;
	}

	public virtual float GetSpeed()
	{
		return game.GetSpeed();
	}

	public virtual void Tick()
	{
		Position = new Vector3 (
			Position.x+Velocity.x*GetSpeed(),
			Position.y+Velocity.y*GetSpeed()
			,Position.z
		);
		Velocity += Acceleration*GetFriction()-Velocity*GetFriction();

		if (DoesCollide ()) {


					foreach (entityBase Stocking in game.gameEntities) {

				if (Stocking != this && Stocking.DoesCollide()) {
						
					
							onCollide (Stocking);
						}
					

			}
		}

		Draw ();
	}

	public virtual bool DoesCollide()
	{
		return false;
	}

	public virtual void Die()
	{
		Velocity = Vector2.zero;
		Acceleration = Vector2.zero;
		isDead = true;
 	}

	public virtual Vector2 GetFlight()
	{return Vector2.zero;
	}

	public virtual float GetMass()
	{
		return 1f;
	}

	public void Move()
	{
		float Speed = GetSpeed ();

		Rect Limits = game.getLimits ();
		bool[] CanMove = new bool[]{true,true};

		float usedRadius = Mathf.Clamp(Radius,0.001f,0.5f);

		if ( Velocity.y > 0) {

			if (Position.y + Velocity.y * Speed> Limits.yMax-usedRadius){
				Position.y =  Mathf.Ceil(Position.y)-usedRadius;
			
				onCollide (0, -1);
				CanMove [1] = false;
			}
		}
		else if (Velocity.y < 0) {
			if (Position.y + Velocity.y * Speed< Limits.yMin+usedRadius){
				Position.y = Mathf.Floor (Position.y)+usedRadius;
				onCollide (0, 1);
				CanMove [1] = false;
			}
		}


		if ( Velocity.x > 0) {

			if (Position.x + Velocity.x * Speed> Limits.xMax-usedRadius){
				Position.x =  Mathf.Ceil(Position.x)-usedRadius;

				onCollide (-1, 0);
				CanMove [0] = false;
			}
		}

		else if (Velocity.x < 0) {
			if (Position.x + Velocity.x * Speed< Limits.xMin+usedRadius){
				Position.x = Mathf.Floor (Position.x)+usedRadius;
				onCollide (1, 0);
				CanMove [0] = false;
			}
		}

		if (CanMove [0] == false) {
			Velocity.x *= 0-GetElasticity();
		}
		if (CanMove [1] == false) {
			Velocity.y *= 0-GetElasticity();
		}
			
		Tick ();
	}

	public virtual float GetElasticity()
	{
		return 0;
	}

	public virtual void Draw()
	{
		gameObject.transform.position = new Vector3(Position.x,Position.y+Position.z*Mathf.Sin(Mathf.PI/2f),-2);
	}
		
	public virtual float GetSpriteScale()
	{
		return 4f;
	}

	public virtual void onCollide(float X, float Y)
	{

	}

	public virtual bool isPlayer()
	{
		return false;
	}

	public virtual void onCollide(entityBase target)
	{

		Vector2 Difference = new Vector2 (
			target.Position.x-Position.x,
			target.Position.y-Position.y
		                     );

		if (Difference.magnitude < target.Radius + Radius) {
			Velocity -= Difference *target.GetMass()/GetMass()/2;
			target.Velocity += Difference*GetMass()/target.GetMass()/2;
		}
	}

	public virtual void onMove()
	{

	}

	public virtual string GetName()
	{
		return "Entity";
	}

	public bool AmIInRangeOfUnit(entityBase Attacker,float Range)
	{
		Vector2 PosA = Attacker.Position;
		Vector2 PosB = Position;

		return (Mathf.Sqrt((PosA.x-PosB.x)*(PosA.x-PosB.x)+(PosA.y-PosB.y)*(PosA.y-PosB.y))<=Range+(Attacker.Radius+Radius)/2);
	}

			public int[] GetMapCoords(){
		return new int[]{Mathf.FloorToInt(Position.x),Mathf.FloorToInt(Position.y)};
			}

	public virtual void DrawSprites(string nSprite)
	{
		Texture2D BaseTexture = DrawTheWorld.LoadTex (nSprite);
		BaseTexture.filterMode = FilterMode.Point;

		SpriteData = new List<UnityEngine.Sprite> {(Sprite.Create (BaseTexture,new Rect(0,0,BaseTexture.width,BaseTexture.height),
			new Vector2(0.5f,0f)))};
	}
	public Vector3 getProjectileLaunchPosition()
	{
		return new Vector3 (Position.x, Position.y, getHeight ());
	}
}
