﻿using System;
using System.Collections.Generic;
using UnityEngine;
	public class HitScan
	{
	Vector2 S;
	Vector2 E;
	Vector2 D;
	float r;
	public float eR = -1f;
	public bool ded=false;
	public List<entityBase> HitUnits;
	entityBase Caster;

	public static HitScan FireFromAngle(StateInGame game, entityBase c, Vector2 Start, float Range, float Angle, float Radius,int MaxTargets, bool CollideWithWalls,float FixedMinDistance)
	{
		return new HitScan (game,c,  Start,Start- new Vector2 (Range*Mathf.Sin(Angle/180*Mathf.PI),Range*Mathf.Cos(Angle/180*Mathf.PI)), Radius, MaxTargets, CollideWithWalls,FixedMinDistance);
	}

	public Vector2 GetStartPosition()
	{
			return S;
		
	}

	public Vector2 GetEndPosition(bool absolute)
	{if (absolute) {
			return E;
		}
		return new Vector2 (E.x *0.99f, E.y *0.99f);
	}

	public HitScan (StateInGame game, entityBase c,Vector2 Start, Vector2 End, float Radius,int MaxTargets, bool CollideWithWalls,float FixedMinDistance)
		{

		D = new Vector2 (
			(Start.x-End.x),
			(Start.x-End.y)).normalized;
		S = new Vector2(Start.x,Start.y);
		E = new Vector2(End.x,End.y);
		r = Radius;
		Caster = c;
		Rect Limits = game.getLimits ();
		float Descale = 1;
		//If collide with walls, see where we end
		if (CollideWithWalls) {

			if (D != Vector2.zero) {
				for (Vector2 vPos = S; (!ded && (vPos - S).magnitude < ((E - S).magnitude)); vPos += D) {


						Vector2 vDelta = new Vector2 (
							                  vPos.x - Mathf.Floor (vPos.x) + D.x,
							                  vPos.y - Mathf.Floor (vPos.y) + D.y
						                  );


					if (vPos.y<Limits.yMin && D.y < 0) {
						float MaxY = Limits.yMin ;
							Descale = (MaxY - S.y) / (E.y - S.y);
							eR = 0;
					} else if (vPos.y>Limits.yMax && D.y > 0) {

						float MaxY = Limits.yMax;
							Descale = (MaxY - S.y) / (E.y - S.y);
							eR = 2;
						}

					if (vPos.x<Limits.xMin && D.x < 0) {
						float MaxX = Limits.xMin;
							Descale = (MaxX - S.x) / (E.x - S.x);
							eR = 1;
					} else if (vPos.x>Limits.xMax && D.x > 0) {

						float MaxX = Limits.xMax;
							Descale = (MaxX - S.x) / (E.x - S.x);
							eR = 3;
						}

					if (Descale < 1) {

						E = new Vector2 (
							S.x + (E.x - S.x) * Descale,
							S.y + (E.y - S.y) * Descale
						);
						ded = true;
					}
				}
			}
		}

		//See what entities we collide with
		if (MaxTargets > 0) {

			Rect collisionRect = new Rect (
				                     Mathf.Min (Start.x - Radius, E.x - Radius),
				                     Mathf.Min (Start.y - Radius, E.y - Radius),
				                     Mathf.Abs (Start.x - E.x) + 2 * Radius,
				                     Mathf.Abs (Start.y - E.y) + 2 * Radius);
		
			HitUnits = new List<entityBase> ();
			foreach (entityBase aData in game.gameEntities) {
			
				if (aData.TakeProjectiles () && !aData.isFriendly(Caster)) {
					Vector2 vPos =new Vector2(aData.Position.x,aData.Position.y); 



					if (
						(End-vPos).magnitude<Radius+aData.Radius || 
						(
							false//Mathf.Abs(Mathf.Sin(Mathf.Atan2(End.y-Start.y,End.x-Start.x)-Mathf.Atan2(vPos.y-Start.y,vPos.x-Start.x))*(vPos-Start).magnitude)<Radius+aData.Radius
						)
					
					) {
							HitUnits.Add (aData);
						}

				}
			}
			HitUnits.Sort (

				delegate (entityBase A, entityBase B) {
					float dA = ((Vector2)A.Position - S).magnitude;
					float dB = ((Vector2)B.Position - S).magnitude;
					return dA.CompareTo (dB);
				}
			);

			HitUnits.Remove (Caster);
			if (FixedMinDistance > 0) {
				foreach (entityBase uUnit in HitUnits) {
					if (((Vector2)uUnit.Position - S).magnitude <= FixedMinDistance) {
						MaxTargets++;
					}
				}
			} 
				while (HitUnits.Count > MaxTargets) {
					HitUnits.RemoveAt (HitUnits.Count - 1);
				}
			

			if (HitUnits.Count > 0) {

				entityBase endTarget = HitUnits [HitUnits.Count - 1];

				Descale =(((Vector2)endTarget.Position-Start).magnitude)/(End-Start).magnitude;

				E = new Vector2 (
					S.x + (E.x - S.x) * Descale,
					S.y + (E.y - S.y) * Descale
				);
				eR = -1;
				ded = true;

			}
		}
	}
}