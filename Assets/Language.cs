﻿
using System;

	public class Language
	{
	public string Sponsor;
	public string[] SponsorMessages;
	public string[] Tips;

	public string[] ProductNames;
	public string[][][] ProductAds;

		public Language ()
	{

		ProductNames = new string[] {
			"Insaniade!",//orange
			"Insaniade! Power",//pink
			"Insaniade! Rampage",//blood red
			"Insaniade! Andrenaline",//black
			"Insaniade! Speed",//silver
			"Insaniade! X",//black and green
			"Insaniade! Plus",//white and blue
			"Insaniade! Turbo",//red+yellow
		};

		ProductAds = new string[][][] {

			new string[][]{
				new string[] {
					"The zany drink cool kidz enjoy",
					"Our segmented market ensures",
				},
				new string[] {
					"No longers causes horrible mutations",
					"We promise ;)",
				},
				new string[] {
					"Don't feed to spiders...",
					"...Seriously, don't",
				},
				new string[] {
					"No longer classified as a harmful substance",
					"Or fit for consumption",
				},
				new string[] {
					"Gives you super powers",
					"Disaster ensues",
				},
				new string[] {
					"The drink that gives you wings",
					"Extra limbs and, possibly, a second head",
				},
				new string[] {
					"We were not responsible for the Novermber mutant disaster",
					"And we have the court files to prove it",
				},
				new string[] {
					"That's the way I like it baby",
					"I don't wanna live forever",
				},
				new string[] {
					"Renders any life ensurance null and void",
					"But who cares? Have another!",
				},
				new string[] {
					"Does not cause mutations in children...",
					"They are called 'improvements'",
				},
				new string[] {
					"Causes 3 types of cancer",
					"Down from 5",
				},
				new string[] {
					"We don't promote mass shootings",
					"Or condone them, for that matter",
				},
			},
			new string[][]{
				new string[] {
					"Fast fire and reload",
					"Have fun!",
				},
				new string[] {
					"Fast fire and reload",
					"Your enemies will pay!",
				},
				new string[] {
					"Fast fire and reload",
					"Now where's that highschool bully?",
				},
			},
			new string[][]{
				new string[] {
					"2X Damage",
					"And enemies explode",
				},
				new string[] {
					"2X Damage",
					"Fuck yeah",
				},
				new string[] {
					"2X Damage",
					"Revenge day",
				},
				new string[] {
					"2X Damage",
					"AHAHAHAHA!",
				},
			},
			new string[][]{
				new string[] {
					"Spray and Pray",
					"Keep your fingers on the trigger",
				},
				new string[] {
					"Spray and Pray",
					"Fingers ithcing?",
				},
				new string[] {
					"Spray and Pray",
					"It's raining lead!",
				},
				new string[] {
					"Spray and Pray",
					"Let the cleaning crew deal with them",
				},
			},
			new string[][]{
				new string[] {
					"Regeneration",
					"Limbs, vitals, anything",
				},
				new string[] {
					"Regeneration",
					"Mother nature cries",
				},
				new string[] {
					"Regeneration",
					"Does not heal a broken heart",
				},
			},
			new string[][]{
				new string[] {
					"Bullet Time",
					"This is awesome!",
				},
				new string[] {
					"Bullet Time",
					"Can't stop this!",
				},
			},
			new string[][]{
				new string[] {
					"Berserk",
					"Woah there, buddy",
				},
				new string[] {
					"Berserk",
					"Deal away with depression!",
				},
				new string[] {
					"Berserk",
					"It's like that dream you had",
				},
			},
			new string[][]{
				new string[] {
					"Addiction",
					"All stats up",
				},
				new string[] {
					"Addiction",
					"First one is always free",
				},
				new string[] {
					"Addiction",
					"You're one of the kool kidz now",
				},
			},
			new string[][]{
				new string[] {
					"Time Trouble",
					"Do not abuse",
				},
				new string[] {
					"Time Trouble",
					"Aren't they peaceful?",
				},
			},

		};

		Sponsor="The Government";
		SponsorMessages = new string[] {
			Sponsor+" is a proud sponsor of "+ProductNames[0],
			Sponsor+" would like to remind you: none of the PAID actors have been harmed in the production of this show!",
			Sponsor+" would like to remind you: we now take newborns as a valid form of currency. Come buy your "+ProductNames[0]+" now!",
			Sponsor+" advises against leaving a can of "+ProductNames[0]+" on the night stand overnight."

		};
	}
	}

