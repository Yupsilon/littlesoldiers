﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class InGameGUI
{
	public GameResources game;
	public float lastClickTime=0f;
	public Canvas GUICanvas;
	public List<GameObject> GUIData;
	public NewMenuWindow Window;
	public property_GUI MenuSkin = new property_GUI ();

	public GUIStyle InfoTitle;
	public GUIStyle InfoButton;
	public GUIStyle ScrollBar;

	public GUIStyle LoadSaveBG;
	public GUIStyle LoadSaveLabel;
	public GUIStyle LoadSaveInput; 

	public InGameGUI(GameResources game)
	{
		game.gui = this;
		this.game = game;

		GUICanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
		GUICanvas.transform.position = new Vector3 (0, 0, 0);
		GUICanvas.gameObject.GetComponent<UnityEngine.UI.CanvasScaler> ().referenceResolution = NewMenus.ScreenResolution;

		GUIData = new List<GameObject> ();


		InfoTitle = new GUIStyle ();
		InfoTitle.normal.background = game.LoadTexture(GameDirectory.UIFile,"MainMenuUI") as Texture2D;
		InfoTitle.normal.background.filterMode = FilterMode.Point;
		InfoTitle.font = Resources.Load ("GUI/Fonts/MainFont") as Font;
		InfoTitle.fontSize = 10;
		InfoTitle.alignment = TextAnchor.MiddleCenter;		
		InfoTitle.normal.textColor=Color.black;

		InfoButton = new GUIStyle ();
		InfoButton.normal.background = game.LoadTexture(GameDirectory.UIFile,"MainMenuUI") as Texture2D;
		InfoButton.normal.background.filterMode = FilterMode.Point;
		InfoButton.font = Resources.Load ("GUI/Fonts/MainFont") as Font;
		InfoButton.fontSize = 10;
		InfoButton.alignment = TextAnchor.MiddleLeft;		
		InfoButton.normal.textColor=Color.black;

		LoadSaveBG=InfoButton;
		LoadSaveLabel=InfoButton;
		LoadSaveInput=InfoButton;
		ScrollBar=InfoButton;
	}

	public void OpenWindow(NewMenuWindow Object)
	{
		Clear ();
		Window=Object;
	}

	public void Clear() {			

		if (Window != null) {			
			Window.Close ();
			Window = null;
		}
	}

	public bool isMouseOverUI()
	{
		if (Window != null) {
			return true;
		}
		return false;
	}
}

