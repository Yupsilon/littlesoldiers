﻿/*
In game currency - cash
Little numbers pop when you kill an enemy
Weapon drops from minibosses and endbosses
Cash grants sponsorship bar - insaniade ultimate
Viewers - permanent currency, roguelike, used to buy enemies, elements, stages, characters
New weapon; deploy
Deploy wall
Deploy turret
Deploy enforcer (robot that follows, shoots bullets at half turret rate)
Hornet; like enforcer, flies, half hp, cannot use regular ammo
Decoy (high focus target)

Build Custom Weapons
Random Gen Dungeons
Crouching
Minidashes
Quests gives bonuses
Drop from the sky

Rivals:
Maze Boss
Portal Boss
Ceiling boss
Octopus that swallows terrain and attacks from the depths

Weapons
Melee/Shockwave
Pulse Pistol
Rifle - hitscan
Rockets
Nade Launcher
Laser
Deploy

Damage Types
	Normal
		-Knockback 80%d - 100% kb
		-Knockback 70%d - 200% kb
		-Knockback Self -140%d
	Fire
		-80%d, 20% chance to 40%
		-20%d, 100% chance to 80%
		-90%d, 5% chance to 100%
	Acid
		-same as fire
	Freeze
	 - 50%d, 50%frost
	 - 20%d, 80%frost
	 - 0%d, 100%frost
	Electric
	 - Same as freeze
	Special
	 - Black hole
	 - Bees

Execution Bonuses

Characters:

Characters
Shift - special
Ult - insaniade
 
Cadet
Passive - revenge (score enough kills/cash to revive)
Start with rifle
Shift - dash
Taunt - the bird - enrage
Ult - explosive ammo clip
 
Rocketgirl
Start with pistol
Passive - slide, imm to self explosions
Shift - slow time
Taunt - flash - stun
Ult - Stop all game speed
 
Kid Wonder
Start with laser, deploy
Passive
Shift - blocking projectile
Ult - half time, double speed
 
Cyborg
Start with rifle and glaunchrr
Passive - extra hp, dual wield, slow walk
Shift - altfire
Ult - no ammo consumption
 
Creature
Start with glove
Passive - imm to kb, extra hp, jump slams
Shift - ghost
Ult - regen and instagib melees
 
Angel
Passive - no hp, halo, immunity after taking hp, slow time when hurt
Start with melee
Shift - temp imm
Ult - refill hp, long invul and forcepush

Scout
-Pulse Pistol
-Super: Bullet Time and Damage Bonus (pistols)

?
-Pulse Rifle
-Instant Reload (on rifles), unlimited ammo
	
Soldier
-Rifle
-Super: 2x Damage + enemies explode (rifles only)

Berserker
-Shotgun
-Regen and gibs, melee only

Rocketeer
- Rocket Launcher
- Time stopped entirely (rockets too)


*/


/* - MILESTONES



STEAl GUN CREATION FROM LOADOUT
RANDOM ITEMS FROM ISAAC
RANDOM ENEMY GEN FROM ISAAC - THOUGHER BOSSES
WORLD UPGRADE FROM HYPDIM

ITEM DROPS ON END LEVEL
*/