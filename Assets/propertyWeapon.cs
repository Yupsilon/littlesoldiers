﻿using System;
using UnityEngine;
using System.Collections.Generic;

	public class propertyWeapon
{
	public const int angleDeltaBase=30;

	public enum WeaponType {
		SquirtGun=0,
		BubbleGun=1,
		Rocket=2
	}
	public bool isBetterVersion=false; //temp

	WeaponType wType = WeaponType.SquirtGun;
	public int Clip=0;
	public void Fire (StateInGame game, entityPlayer launcher,float angle)
	{
		if (Clip > 0) {
			float radStart = 0;
			float radDelta = 0;
			float radAdd = angleDeltaBase * GetProjectileSpreadMultiplier ();

			if (GetNumProjectiles () > 1) {
				radStart = radAdd * (GetNumProjectiles ()-1) / 2f;
					radDelta = radAdd;
			}

			for (int iP = 0; iP < GetNumProjectiles (); iP++) {
				entityProjectile Zim;
				switch (wType) {
				case WeaponType.SquirtGun:
					Zim = entityProjectile.ShootProjectile (game, launcher, "bullet_default", GetRange () / GetProjectileVelocity (), angle + radStart - radDelta * iP, GetProjectileVelocity (), GetAccuracy (), GetProjectileRadius (), 1, launcher.squirtColor);

					Zim.controller.AddFunction (propertyModifier.actions.collide, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						Zim.Die ();
					});

					Vector2 lastpondPos = Vector2.zero;
					Zim.controller.thinkInterval = 0.04f;
					Zim.controller.AddFunction (propertyModifier.actions.think, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						if (GetSplashRadius () > 0 && ((Vector2)Zim.Position-lastpondPos).magnitude*DrawTheWorld.DefaultTileSize/2f>GetSplashRadius ()) {
							game.generatePuddle (new Vector2 (Zim.Position.x, Zim.Position.y), GetSplashRadius (), launcher.squirtColor, 0);
							lastpondPos=Zim.Position;
						}
					});
					Zim.controller.AddFunction (propertyModifier.actions.attack, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						Zim.Die ();
					});

					Zim.controller.AddFunction (propertyModifier.actions.expire, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						entitySpecialEffect.MakeExplosion (game,(Vector2) effect.Position - effect.Velocity.normalized * effect.Radius, 0, 0, "explosion_bullet", false,-1,effect.squirtColor);


					});

					break;
				case WeaponType.BubbleGun:
					Zim = entityProjectile.ShootProjectile (game, launcher, "bullet_bubble", GetRange () / GetProjectileVelocity (), angle + radStart - radDelta * iP, GetProjectileVelocity (), GetAccuracy (), GetProjectileRadius (), 1, launcher.squirtColor);

					Zim.controller.AddFunction (propertyModifier.actions.attack, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						Zim.Die ();
					});


					Zim.controller.AddFunction (propertyModifier.actions.expire, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						entitySpecialEffect.MakeExplosion (game,(Vector2) effect.Position - effect.Velocity.normalized * effect.Radius, 0, 0, "explosion_bubble", false,-1,effect.squirtColor);

						if (GetSplashRadius () > 0) {
							game.generatePuddle (new Vector2 (Zim.Position.x, Zim.Position.y), GetSplashRadius ()*1.25f, launcher.squirtColor, 0);
							lastpondPos=Zim.Position;
						}

					});
					break;
				case WeaponType.Rocket:


					Zim = entityProjectile.ShootProjectile (game, launcher, "bullet_rocket", GetRange () / GetProjectileVelocity (), angle + radStart - radDelta * iP, GetProjectileVelocity (), GetAccuracy (), GetProjectileRadius (), 1, launcher.squirtColor);

					Zim.controller.AddFunction (propertyModifier.actions.attack, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						Zim.Die ();
					});

					Zim.controller.AddFunction (propertyModifier.actions.collide, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {
						game.generatePuddle (new Vector2 (Zim.Position.x, Zim.Position.y), GetSplashRadius ()*2/3f, launcher.squirtColor, 0);
					});

					Zim.controller.AddFunction (propertyModifier.actions.expire, delegate(propertyModifier Mod, entitySpecialEffect effect, entityActor target) {

						(effect as entityProjectile).explode("explosion_rocket",GetSplashRadius());

						if (GetSplashRadius () > 0) {
							game.generatePuddle (new Vector2 (Zim.Position.x, Zim.Position.y), GetSplashRadius ()/4f,9, launcher.squirtColor, 0);
							lastpondPos=Zim.Position;
						}

					});
					break;
				}
			}
			launcher.AddModifier (game, new propertyModifier ("recoil",propertyModifier.Allignment_neutral, GetRecoilTime (), new int[]{ propertyModifier.modstate_Disarmed }), 0);

			Clip--;
		}
		if (Clip <= 0) {
			Reload (launcher);
			//launcher.ChangeWeapon();
		}
	}

	public void Reload(entityPlayer launcher)
	{
		propertyModifier RelMod = new propertyModifier ("reload",propertyModifier.Allignment_neutral, GetReloadTime (), new int[]{ propertyModifier.modstate_Disarmed }); 
		launcher.AddModifier (launcher.game,RelMod , 0);

		RelMod.AddFunction(propertyModifier.actions.expire,delegate {
			Clip=GetClipSize();
		});

		launcher.squirtColor = launcher.squirtColor;
	}

	public float GetNumProjectiles()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return 1*GetProjectileMultiplier();
		case WeaponType.BubbleGun:
			return 2*GetProjectileMultiplier();
		case WeaponType.Rocket:
			return 1*GetProjectileMultiplier();
		}

		return 1;
	}

	public float GetProjectileMultiplier()
	{
		if (wType == WeaponType.BubbleGun && isBetterVersion) {
			return 2f;
		}
		if (wType == WeaponType.SquirtGun && isBetterVersion) {
			return 6f;
		}
		return 1;
	}

	public float GetRecoilTime()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return 0.04f*GetRecoilMultiplier();
		case WeaponType.BubbleGun:
			return 0.2f*GetRecoilMultiplier();
		case WeaponType.Rocket:
			return 0.25f*GetRecoilMultiplier();
		}

		return 1;
	}

	public float GetRecoilMultiplier()
	{
		if (wType == WeaponType.SquirtGun && isBetterVersion) {
			return 18f;
		}
		return 1;
	}

	public float GetRange()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return 12*GetRangeMultiplier();
		case WeaponType.BubbleGun:
			return 16*GetRangeMultiplier();
		case WeaponType.Rocket:
			return 9f*GetRangeMultiplier();
		}

		return 1;
	}

	public float GetRangeMultiplier()
	{
		if (wType == WeaponType.Rocket && !isBetterVersion) {
			return 2.5f;
		}
		return 1;
	}

	public float GetAccuracy()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return 7*GetAccuracyModifier();
		case WeaponType.BubbleGun:
			return 0;
		case WeaponType.Rocket:
			return 0;
		}

		return 1;
	}
		
	public float GetAccuracyModifier()
	{
		return 1;
	}

	public float GetProjectileRadius()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return .25f;
		case WeaponType.BubbleGun:
			return .33f;
		case WeaponType.Rocket:
			return .5f;
		}

		return 1;
	}

	public float GetProjectileVelocity()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return 20*GetVelocityMultiplier();
		case WeaponType.BubbleGun:
			return 14*GetVelocityMultiplier();
		case WeaponType.Rocket:
			return 15f*GetVelocityMultiplier();
		}

		return 1;
	}

	public float GetVelocityMultiplier(){
		return 1;
	}

	public int GetClipSize()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return Mathf.RoundToInt(10*GetAmmoMultiplier());
		case WeaponType.BubbleGun:
			return  Mathf.RoundToInt(10*GetAmmoMultiplier());
		case WeaponType.Rocket:
			return  Mathf.RoundToInt(1*GetAmmoMultiplier());
		}

		return 1;
	}

	public float GetAmmoMultiplier(){
		if (wType == WeaponType.SquirtGun && isBetterVersion) {
			return 1/5f;
		}
		if (wType == WeaponType.BubbleGun && isBetterVersion) {
			return 1/2f;
		}
		if (wType == WeaponType.Rocket && isBetterVersion) {
			return 6f;
		}
		return 1;
	}

	public float GetReloadTime()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return 6/5f*GetReloadMultiplier();
		case WeaponType.BubbleGun:
			return 7/4f*GetReloadMultiplier();
		case WeaponType.Rocket:
			return 3*GetReloadMultiplier();
		}

		return 1;
	}

	public float GetReloadMultiplier()
	{
		if (wType == WeaponType.SquirtGun && isBetterVersion) {
			return 2f;
		}
		if (wType == WeaponType.Rocket && isBetterVersion) {
			return 3/5f;
		}
		return 1;
	}

	public float GetDamageMultiplier()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return 2*GetSplashMultiplier();
		case WeaponType.BubbleGun:
			return 3*GetSplashMultiplier();
		case WeaponType.Rocket:
			return 4*GetSplashMultiplier();
		}

		return 1;
	}

	public float GetSplashRadius()
	{
		switch (wType) {
		case WeaponType.SquirtGun:
			return 8*GetSplashMultiplier();
		case WeaponType.BubbleGun:
			return 12*GetSplashMultiplier();
		case WeaponType.Rocket:
			return 22*GetSplashMultiplier();
		}

		return 1;
	}

	public float GetSplashMultiplier()
	{
		if (wType == WeaponType.Rocket && !isBetterVersion) {
			return 3f;
		}
		return 1f;
	}

	public float GetProjectileSpreadMultiplier()
	{
		if (wType == WeaponType.BubbleGun) {
			return 1f/3f;
		}
		if (wType == WeaponType.SquirtGun && isBetterVersion) {
			return 1/20f;
		}
		return 1f;
	}

	public static propertyWeapon Custom(WeaponType gun, bool upgrade){
		propertyWeapon Gun = new propertyWeapon ();

		Gun.wType = gun;
		Gun.isBetterVersion = upgrade;
		Gun.Clip = Gun.GetClipSize (); 

		return Gun;
	}

	public static propertyWeapon Random(){
		propertyWeapon Gun = new propertyWeapon ();

		switch (Mathf.RoundToInt (UnityEngine.Random.Range (0, 5))) {
		case 0:
			Gun.wType = WeaponType.SquirtGun;
			break;
		case 1:
			Gun.wType = WeaponType.SquirtGun;
			Gun.isBetterVersion = true;
			break;
		case 2:
			Gun.wType = WeaponType.BubbleGun;
			break;
		case 3:
			Gun.wType = WeaponType.BubbleGun;
			Gun.isBetterVersion = true;
			break;
		case 5:
			Gun.wType = WeaponType.Rocket;
			break;
		case 4:
			Gun.wType = WeaponType.Rocket;
			Gun.isBetterVersion = true;
			break;
		}
		Gun.Clip = Gun.GetClipSize (); 

		return Gun;
	}
	}


