﻿using System;
using UnityEngine;
using System.Collections.Generic;

	public class entityAI
{
	public static string nThink="t";
	public static string nCollide="cw";
	public static string nAttack="a";
	public static string nHit="h";

	entityPlayer Owner;
	public delegate void AiThink();

	public List<string> FuncNames = new List<string> ();
	public List<entityBase> PossibleTargets = new List<entityBase> ();
	public List<AiThink> functions = new List<AiThink>();
	public float tInterval=0;
	public float lastUpdate = 0f;

	public entityAI(entityPlayer entity, float think)
	{
		Owner = entity;
		tInterval = think;
	}

	public entityBase GetBestTarget()
	{if (PossibleTargets.Count == 0 || PossibleTargets [0].alignment == Owner.alignment) {
			UpdateTargets ();
		}
		if (PossibleTargets.Count == 0) {
			return null;
		}
		PossibleTargets.Sort ((a, b) => GetPriority (a).CompareTo (GetPriority (b)));

		return PossibleTargets [0];
	}

	public void UpdateTargets()
	{
		PossibleTargets.Clear ();
		foreach (entityBase eData in Owner.game.gameEntities) {
			if (eData.alignment > 0) {
				PossibleTargets.Add (eData);
			}
		}
	}

			public int GetPriority(entityBase target)
	{
		return 1;
	}

	public void Update()
	{
		if (Owner.game.gameTime > lastUpdate) {
			lastUpdate = Owner.game.gameTime + tInterval;

			ExecuteFunction (nThink);
		}
	}

	public void AddFunction(string Name, AiThink execution)
	{
		if (execution == null) {
			return;}
		if (FuncNames.Contains (Name)) {
			functions[FuncNames.IndexOf (Name)]=execution;
		} else {
			FuncNames.Add (Name);
			functions.Add (execution);
		}
	}

	public void ExecuteFunction(string Name){

		if (FuncNames.IndexOf (Name) >= 0 && FuncNames.IndexOf (Name) < functions.Count) {
			if (functions[FuncNames.IndexOf (Name)] != null) {

				functions [FuncNames.IndexOf (Name)] ();
			}
		}
	}
}

